import Vue from 'vue'
import App from './App'
import store from './store'
import uView from 'uview-ui'
import http from './Api/index'
// import 'uview-ui/index.css'
Vue.use(uView)
console.log(process.env.NODE_ENV)
Vue.config.productionTip = false
Vue.prototype.$http = http
App.mpType = 'app'

const app = new Vue({
  ...App,
  store
})
app.$mount()
